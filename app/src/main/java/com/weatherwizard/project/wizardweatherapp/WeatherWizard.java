package com.weatherwizard.project.wizardweatherapp;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import com.google.gson.*;

import javax.swing.*;

public class WeatherWizard {
    private final String ACCESS_TOKEN = "15f35ebeb2da5499";
    private JsonElement jse;
    //private ImageIcon radar;

    // constructor
    public WeatherWizard (String city, String stateOrCountry, String zip)
    {
        city = city.replaceAll(" ", "_").toLowerCase();

        stateOrCountry = stateOrCountry.replaceAll(" ", "_");

        jse = null;
        //radar = null;

        URL wGroundURL;
        URL wGroundRadarURL;

        try
        {
            if(zip.equals(""))
            {
                // Construct weather underground URL
                System.out.println("getting URL");
                wGroundURL = new URL("http://api.wunderground.com/api/"
                        + ACCESS_TOKEN + "/conditions/forecast10day/astronomy/q/"
                        + stateOrCountry + "/" + city + ".json");
            }
            else
            {
                System.out.println("getting url");
                wGroundURL = new URL("http://api.wunderground.com/api/"
                        + ACCESS_TOKEN + "/conditions/forecast10day/astronomy/q/"
                        + zip + ".json");
            }

            // Open the URL
            InputStream is = wGroundURL.openStream(); // throws an IOException
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            // Read the result into a JSON Element
            jse = new JsonParser().parse(br);

            // Close the connection
            is.close();
            br.close();
        }
        catch (java.io.IOException ioe)
        {
            ioe.printStackTrace();
        }

        if(checkValidInput())
        {
            try
            {
                if(getCountry().equals("US"))
                {
                    System.out.println("getting radar URL");
                    wGroundRadarURL = new URL("http://api.wunderground.com/api/" + ACCESS_TOKEN
                            + "/animatedsatellite/animatedradar/q/"
                            + getZip() + ".gif?num=6&delay=50&interval=30");
                }
                else
                {
                    System.out.println("getting radar url");
                    wGroundRadarURL = new URL("http://api.wunderground.com/api/" + ACCESS_TOKEN
                            + "/animatedsatellite/animatedradar/q/"
                            + getCountry() + "/" + getCity() + ".gif?num=6&delay=50&interval=30");
                }

                //radar = new ImageIcon(wGroundRadarURL);

            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            //radar = null;
        }

    }

    // if user input is valid, returns true
    // this method needs getCity();
    public boolean checkValidInput() {
        boolean flag = true;
        try {
            getCity();
        } catch (java.lang.NullPointerException npe) {
            flag = false;
        }

        return flag;
    }

    // -------------------------------------RADAR/SATELLITE---------------------------------------------------------

    /*
    public ImageIcon getRadar()
    {
        return radar;
    }
    */

    // -------------------------------------CURRENT CONDITIONS -----------------------------------------------------

    public double getTempF()
    {
        return jse.getAsJsonObject().get( "current_observation" )
                .getAsJsonObject().get( "temp_f" )
                .getAsDouble();
    }

    public double getTempC()
    {
        return jse.getAsJsonObject().get( "current_observation" )
                .getAsJsonObject().get( "temp_c" )
                .getAsDouble();
    }

    public String getWeather()
    {
        return jse.getAsJsonObject().get( "current_observation" )
                .getAsJsonObject().get( "weather" )
                .getAsString();
    }

    public String getCityState()
    {
        return jse.getAsJsonObject().get("current_observation")
                .getAsJsonObject().get("display_location")
                .getAsJsonObject().get("full")
                .getAsString();
    }

    public String getCountry()
    {
        return jse.getAsJsonObject().get("current_observation")
                .getAsJsonObject().get("observation_location")
                .getAsJsonObject().get("country")
                .getAsString();
    }

    public String getState()
    {
        return jse.getAsJsonObject().get("current_observation")
                .getAsJsonObject().get("display_location")
                .getAsJsonObject().get("state")
                .getAsString();
    }

    public String getCity()
    {
        return jse.getAsJsonObject().get("current_observation")
                .getAsJsonObject().get("display_location")
                .getAsJsonObject().get("city")
                .getAsString();
    }

    public int getZip()
    {
        return jse.getAsJsonObject().get("current_observation")
                .getAsJsonObject().get("display_location")
                .getAsJsonObject().get("zip")
                .getAsInt();
    }

    public String getHumidity()
    {
        return jse.getAsJsonObject().get( "current_observation" )
                .getAsJsonObject().get( "relative_humidity" )
                .getAsString();
    }

    public double getWindSpeedMPH()
    {
        return jse.getAsJsonObject().get( "current_observation" )
                .getAsJsonObject().get( "wind_mph" )
                .getAsDouble();
    }

    public double getWindSpeedKPH()
    {
        return jse.getAsJsonObject().get( "current_observation" )
                .getAsJsonObject().get( "wind_kph" )
                .getAsDouble();
    }

    public double getWindGustMPH()
    {
        return jse.getAsJsonObject().get( "current_observation" )
                .getAsJsonObject().get( "wind_gust_mph" )
                .getAsDouble();
    }

    public double getWindGustKPH()
    {
        return jse.getAsJsonObject().get( "current_observation" )
                .getAsJsonObject().get( "wind_gust_kph" )
                .getAsDouble();
    }

    public String getWindchillF()
    {
        return jse.getAsJsonObject().get( "current_observation" )
                .getAsJsonObject().get( "windchill_f"
                ).getAsString();
    }

    public String getWindchillC()
    {
        return jse.getAsJsonObject().get( "current_observation" )
                .getAsJsonObject().get( "windchill_c" )
                .getAsString();
    }

    public String getWindDirection()
    {
        return jse.getAsJsonObject().get("current_observation")
                .getAsJsonObject().get("wind_dir")
                .getAsString();
    }

    // ------------------------------------ 10 DAY FORECAST ---------------------------------------------------

    public double get10DayHighF( int i )
    {
        if ( i > -1 && i < 10 )
        {
            return jse.getAsJsonObject().get( "forecast" )
                    .getAsJsonObject().get( "simpleforecast" )
                    .getAsJsonObject().get("forecastday")
                    .getAsJsonArray().get( i )
                    .getAsJsonObject().get("high")
                    .getAsJsonObject().get("fahrenheit")
                    .getAsDouble();
        }
        return 0;
    }

    public double get10DayHighC( int i )
    {
        if ( i > -1 && i < 10 )
        {
            return jse.getAsJsonObject().get( "forecast" )
                    .getAsJsonObject().get( "simpleforecast" )
                    .getAsJsonObject().get("forecastday")
                    .getAsJsonArray().get( i )
                    .getAsJsonObject().get("high")
                    .getAsJsonObject().get("celsius")
                    .getAsDouble();
        }
        return 0;
    }

    public double get10DayLowF( int i )
    {
        if ( i > -1 && i < 10 )
        {
            return jse.getAsJsonObject().get( "forecast" )
                    .getAsJsonObject().get( "simpleforecast" )
                    .getAsJsonObject().get("forecastday")
                    .getAsJsonArray().get( i )
                    .getAsJsonObject().get("low")
                    .getAsJsonObject().get("fahrenheit")
                    .getAsDouble();
        }
        return 0;
    }

    public double get10DayLowC( int i )
    {
        if ( i > -1 && i < 10 )
        {
            return jse.getAsJsonObject().get( "forecast" )
                    .getAsJsonObject().get( "simpleforecast" )
                    .getAsJsonObject().get("forecastday")
                    .getAsJsonArray().get( i )
                    .getAsJsonObject().get("low")
                    .getAsJsonObject().get("celsius")
                    .getAsDouble();
        }
        return 0;
    }

    public double get10DayPrecip( int i )
    {
        if ( i > -1 && i < 10 )
        {
            return jse.getAsJsonObject().get( "forecast" )
                    .getAsJsonObject().get( "simpleforecast" )
                    .getAsJsonObject().get("forecastday")
                    .getAsJsonArray().get( i )
                    .getAsJsonObject().get("pop")
                    .getAsDouble();
        }
        return 0;
    }

    public int get10DayDayNumber( int i )
    {
        if ( i > -1 && i < 10 )
        {
            return jse.getAsJsonObject().get( "forecast" )
                    .getAsJsonObject().get( "simpleforecast" )
                    .getAsJsonObject().get("forecastday")
                    .getAsJsonArray().get( i )
                    .getAsJsonObject().get("date")
                    .getAsJsonObject().get("day")
                    .getAsInt();
        }
        return 0;
    }

    public int get10DayMonthNumber( int i )
    {
        if ( i > -1 && i < 10 )
        {
            return jse.getAsJsonObject().get( "forecast" )
                    .getAsJsonObject().get( "simpleforecast" )
                    .getAsJsonObject().get("forecastday")
                    .getAsJsonArray().get( i )
                    .getAsJsonObject().get("date")
                    .getAsJsonObject().get("month")
                    .getAsInt();
        }
        return 0;
    }

    public String get10DayDayNameShort( int i )
    {
        if ( i > -1 && i < 10 )
        {
            return jse.getAsJsonObject().get( "forecast" )
                    .getAsJsonObject().get( "simpleforecast" )
                    .getAsJsonObject().get("forecastday")
                    .getAsJsonArray().get( i )
                    .getAsJsonObject().get("date")
                    .getAsJsonObject().get("weekday_short")
                    .getAsString();
        }

        return "";
    }

    public String get10DayDayNameLong( int i )
    {
        if ( i > -1 && i < 10 )
        {
            return jse.getAsJsonObject().get( "forecast" )
                    .getAsJsonObject().get( "simpleforecast" )
                    .getAsJsonObject().get("forecastday")
                    .getAsJsonArray().get( i )
                    .getAsJsonObject().get("date")
                    .getAsJsonObject().get("weekday")
                    .getAsString();
        }

        return "";
    }

    public String get10DayMonthNameShort( int i )
    {
        if ( i > -1 && i < 10 )
        {
            return jse.getAsJsonObject().get( "forecast" )
                    .getAsJsonObject().get( "simpleforecast" )
                    .getAsJsonObject().get("forecastday")
                    .getAsJsonArray().get( i )
                    .getAsJsonObject().get("date")
                    .getAsJsonObject().get("monthname_short")
                    .getAsString();
        }

        return "";
    }

    public String get10DayMonthNameLong( int i )
    {
        if ( i > -1 && i < 10 )
        {
            return jse.getAsJsonObject().get( "forecast" )
                    .getAsJsonObject().get( "simpleforecast" )
                    .getAsJsonObject().get("forecastday")
                    .getAsJsonArray().get( i )
                    .getAsJsonObject().get("date")
                    .getAsJsonObject().get("monthname")
                    .getAsString();
        }

        return "";
    }

    public String get10DayConditions( int i )
    {
        return jse.getAsJsonObject().get( "forecast" )
                .getAsJsonObject().get( "simpleforecast" )
                .getAsJsonObject().get("forecastday")
                .getAsJsonArray().get( i )
                .getAsJsonObject().get("conditions")
                .getAsString();
    }
    /*
    public ImageIcon get10DayIcon( int i) {
        if (i > -1 && i < 20) {
            try {
                return new ImageIcon(
                        new URL(jse.getAsJsonObject().get("forecast")
                                .getAsJsonObject().get("txt_forecast")
                                .getAsJsonObject().get("forecastday")
                                .getAsJsonArray().get(i)
                                .getAsJsonObject().get("icon_url")
                                .getAsString()));
            } catch (MalformedURLException mue) {
                mue.printStackTrace();

            }
        }

        return new ImageIcon("images/fail.png");
    }
    */
    //------------------------------------- ASTRONOMY --------------------------------------------------------

    public int getSunriseHour()
    {
        return jse.getAsJsonObject().get( "moon_phase" )
                .getAsJsonObject().get( "sunrise" )
                .getAsJsonObject().get( "hour" )
                .getAsInt();
    }

    public int getSunriseMinute()
    {
        return jse.getAsJsonObject().get( "moon_phase" )
                .getAsJsonObject().get( "sunrise" )
                .getAsJsonObject().get( "minute" )
                .getAsInt();
    }

    public int getSunsetHour()
    {
        return jse.getAsJsonObject().get( "moon_phase" )
                .getAsJsonObject().get( "sunset" )
                .getAsJsonObject().get( "hour" )
                .getAsInt();
    }

    public int getSunsetMinute()
    {
        return jse.getAsJsonObject().get( "moon_phase" )
                .getAsJsonObject().get( "sunset" )
                .getAsJsonObject().get( "minute" )
                .getAsInt();
    }

    public String getMoonPhase()
    {
        return jse.getAsJsonObject().get( "moon_phase" )
                .getAsJsonObject().get( "phaseofMoon" )
                .getAsString();
    }

}
